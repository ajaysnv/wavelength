trigger WorkorderTrigger on WorkOrder (after insert,after update) {

WorkOrderTriggerHandler helper=new WorkOrderTriggerHandler();

if(Trigger.isinsert && Trigger.isAfter){
helper.OnInsert(Trigger.new);

}
if(Trigger.isupdate && Trigger.isAfter){
helper.OnUpdate(Trigger.new,trigger.oldmap);

}

}
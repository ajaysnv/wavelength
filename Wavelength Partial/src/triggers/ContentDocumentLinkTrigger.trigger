/*
* @Author               : Ajitesh
* @Description          : Apex trigger to invoke clone ContentDocumentLinkHandler
* @Date Of Creation     : Mar 05-2019
* @Modified by          : 
* @Modified Date        : 
* @Modifiction History  :
* @Related Meta Data    : ContentDocumentLinkHandler
*/
trigger ContentDocumentLinkTrigger on ContentDocumentLink (after insert) {    
    ContentDocumentLinkHandler.cloneWorkOrderFiles(trigger.new);
}
trigger CreateAssetonClosedWon on Opportunity (after insert, after update) {
  if(checkRecursive.OppTriggerforcreateAsset==true) 
     for(Opportunity o: trigger.new){ 
      if(o.isWon == true && o.HasOpportunityLineItem == true){
         String opptyId = o.Id;
         OpportunityLineItem[] OLI = [Select OpportunityId , Room_Code__c , isProduct__c ,UnitPrice, Quantity, PricebookEntry.Product2Id, PricebookEntry.Product2.Name, Description, Converted_to_Asset__c  
                                      From OpportunityLineItem 
                                      where OpportunityId = :opptyId and isProduct__c  = true  and Converted_to_Asset__c = false];
         Asset[] ast = new Asset[]{};
         Asset a = new Asset();
         for(OpportunityLineItem ol: OLI){
           
            for(integer Quan=1; Quan<=ol.Quantity;  Quan++){
            a = new Asset();
        a.AccountId = o.AccountId;
            a.Product2Id = ol.PricebookEntry.Product2Id;
            a.Quantity = ol.Quantity;
            a.Price =  ol.UnitPrice;
            a.PurchaseDate = o.CloseDate;
            a.Status = 'Purchased';
            a.Description = ol.Description;
            a.Name = ol.PricebookEntry.Product2.Name;
            a.Room_Code__c  = ol.Room_Code__c; 
            a.Opportunity__c = ol.OpportunityId;
            ast.add(a);
            ol.Converted_to_Asset__c = true;
       }
       }
       checkRecursive.OppTriggerforcreateAsset=false;
      update OLI; 
      
      insert ast;
      
     }
     
    }
}
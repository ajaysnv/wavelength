@isTest class WorkOrderTriggerTest {
    testMethod static void test(){
        Account acc = new Account(Name='Test Account');
        insert acc;
        
        Opportunity opp = new Opportunity(Name='Test Opp',AccountId=acc.Id,StageName='Estimate Review',CloseDate=System.today().addMonths(1),Installation_Earliest_Date__c=System.Now());
        insert opp;
        
        Asset ast = new Asset(Name='testAsset',Opportunity__c=opp.Id,AccountId=acc.Id);
        insert ast;
        
        WorkType wt = new WorkType(Name='TestWT',DurationType='Hours',EstimatedDuration=2);
        insert wt;
        
        WorkOrder wo = new WorkOrder(Number_of_Techs__c=1,Status='New',Opportunity__c=opp.Id,WorkTypeId=wt.Id,MinimumCrewSize=3);
        insert wo;
        
        ServiceAppointment sa = new ServiceAppointment(ParentRecordId=wo.Id,Status='None',EarliestStartTime=System.now(),DueDate=System.now().addMonths(1));
        insert sa;
        
        wo.Number_of_Techs__c =2;
        update wo;
    }
}
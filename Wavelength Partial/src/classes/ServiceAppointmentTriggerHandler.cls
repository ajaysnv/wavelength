//Description:Create SA s based on Workorder Number of techs (can't used Min Crew Size)

public class ServiceAppointmentTriggerHandler {
   /*Commented by Appsolve developer on 28 Aug 2018 as this feature was moved to WO and this class is no longer valid
    Set<id> SetSAIds = new set<id>(); 
    List<ServiceAppointment> listCloneSA=new list<ServiceAppointment>();
    List<ServiceAppointment> listclone=new list<ServiceAppointment>();
    string checkflag='Count';
    
       public  void OnInsert(List<ServiceAppointment> newSA){
           
           for(ServiceAppointment ObjSA:newSA){
               if(ObjSA.Work_Order__c!=null ){
               SetSAIds.add(ObjSA.id);
               
               }
               system.debug('***'+SetSAIds);
               CloneSA();
           }
           }
           
           public  void OnUpdate(List<ServiceAppointment> newSA,Map<id,ServiceAppointment> OldMapSA){
           
           for(ServiceAppointment ObjSA:newSA){
               if(ObjSA.Work_Order__c!=null && OldMapSA.get(ObjSA.id).Work_Order__c==null && OldMapSA.get(ObjSA.id).Work_Order__c!=ObjSA.Work_Order__c){
               SetSAIds.add(ObjSA.id);
               
               }
               system.debug('***1'+ObjSA.Work_Order__c);
               system.debug('***1'+OldMapSA.get(ObjSA.id).Work_Order__c);
               
               system.debug('***1'+SetSAIds);
               CloneSA();
           }
           }
           
    public void CloneSA(){       
             
           for(ServiceAppointment ObjSA:[select id,ParentRecordid,ParentRecordType,Subject,Status,FSL__InternalSLRGeolocation__Latitude__s,FSL__InternalSLRGeolocation__Longitude__s,street,City,
                                         State,country,EarliestStartTime,DueDate,Duration,DurationType,Description,Work_Order__c,Account_Name__c,Accountid,Work_Order__r.MinimumCrewSize,Work_Order__r.Number_of_Techs__c,Account.name from ServiceAppointment where id In:SetSAIds]){
             //  If(!checkRecursive.SetOfIDs.contains(ObjSA.Id)){
               system.debug('***1'+ObjSA);
               if(ObjSA.Work_Order__r.Number_of_Techs__c!=null && ObjSA.Work_Order__r.Number_of_Techs__c>1){
                              system.debug('***2');                   
                   for(integer Crewsize=1; Crewsize<=ObjSA.Work_Order__r.Number_of_Techs__c-1;  Crewsize++){
                       system.debug('***3');
                       
                       ServiceAppointment ObjSAnew=new ServiceAppointment();
                       ObjSAnew.ParentRecordid=ObjSA.ParentRecordId;
                       //ObjSAnew.ParentRecordType=ObjSA.ParentRecordType;
                       ObjSAnew.Subject=ObjSA.Subject;
                       ObjSAnew.State=ObjSA.State;
                       ObjSAnew.FSL__InternalSLRGeolocation__Latitude__s=ObjSA.FSL__InternalSLRGeolocation__Latitude__s;
                       ObjSAnew.FSL__InternalSLRGeolocation__Longitude__s=ObjSA.FSL__InternalSLRGeolocation__Longitude__s;
                       ObjSAnew.street=ObjSA.Street;
                       ObjSAnew.City=ObjSA.City;
                       ObjSAnew.Country=ObjSA.Country;
                       ObjSAnew.EarliestStartTime=ObjSA.EarliestStartTime;
                       ObjSAnew.DueDate=ObjSA.DueDate;
                       ObjSAnew.Duration=ObjSA.Duration;
                       ObjSAnew.DurationType=ObjSA.DurationType;
                       ObjSAnew.Description=ObjSA.Description;
                       ObjSAnew.Work_Order__c=ObjSA.Work_Order__c;
                       ObjSAnew.Account_Name__c=ObjSA.Account_Name__c;
                       ObjSAnew.Staus='None';
                       //ObjSAnew.Account.name=ObjSA.Account.name;
                       
                      listCloneSA.add(ObjSAnew);
                      
                   }  
                   }                   
                                                 
             
               
               
           }
           
           system.debug('*****5'+listCloneSA);
           if(!listCloneSA.isempty()){
                 checkRecursive.isUpdate=false;
                 insert listCloneSA;
               
           }
          
           
 }       

 */  
}
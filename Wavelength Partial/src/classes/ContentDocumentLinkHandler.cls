/*
* @Author               : Ajitesh
* @Description          : Apex class to clone workorder files on associated Account record
* @Date Of Creation     : Mar 05-2019
* @Modified by          : 
* @Modified Date        : 
* @Modifiction History  :
* @Related Meta Data    : 
*/
public class ContentDocumentLinkHandler {
    
    public static void cloneWorkOrderFiles(List<ContentDocumentLink> cdLinkList){       
        
        String woKeyPrefix = WorkOrder.SObjectType.getDescribe().getKeyPrefix();
        Set<Id> workOrderIds = new Set<Id>();
        for(ContentDocumentLink cdLink:cdLinkList){
            if((''+cdLink.LinkedEntityId).startsWith(woKeyPrefix)){
                workOrderIds.add(cdLink.LinkedEntityId);
            }
        }
        
        if(!workOrderIds.isEmpty()){
            Map<Id,WorkOrder> workOrderMap = new Map<Id,WorkOrder>([Select Id, AccountId FROM WorkOrder WHERE Id IN:workOrderIds AND AccountId<>null]);
            
            List<Messaging.SingleEmailMessage> messagesToSend = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail;
            EmailTemplate notificationET = [Select Id from EmailTemplate where DeveloperName=:Label.Workorder_Picture_Uploaded_Notification_Email_Template];
            
            if(Label.Workorder_Picture_Uploaded_Notification!=null){  	
                User user = [select Email, FirstName, LastName FROM User WHERE Id =:Label.Workorder_Picture_Uploaded_Notification];
                Contact tempContact = new Contact(Email = user.Email, FirstName = user.FirstName, LastName = user.LastName);
                insert tempContact;
                
                List<ContentDocumentLink> newCdLinks = new List<ContentDocumentLink>();
                ContentDocumentLink newCdLink;
                for(ContentDocumentLink cdLink:cdLinkList){
                    if(workOrderMap.containsKey(cdLink.LinkedEntityId)){
                        newCdLink = cdLink.clone(false,false,true,true);
                        newCdLink.LinkedEntityId = workOrderMap.get(cdLink.LinkedEntityId).AccountId;
                        newCdLinks.add(newCdLink);
                        
                        if(Label.Workorder_Picture_Uploaded_Notification!=null){
                            mail = new Messaging.SingleEmailMessage(); 
                            mail.setTargetObjectId(tempContact.Id); 
                            mail.setTemplateId(notificationET.id);
                            mail.setwhatId(cdLink.LinkedEntityId); //workorder id
                            mail.setUseSignature(false);
                            mail.setSaveAsActivity(false);    
                            messagesToSend.add(mail);
                        }
                    }
                }                
                
                if(!newCdLinks.isEmpty()){
                    Database.insert(newCdLinks,false);
                    System.debug('newCdLinks @>'+newCdLinks);
                    
                    Messaging.sendEmail(messagesToSend);
                    System.debug('messagesToSend @>'+messagesToSend);
                }
                delete tempContact;
            }
        }    
    }
}
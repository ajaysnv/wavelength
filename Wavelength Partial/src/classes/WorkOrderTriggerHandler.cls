public class WorkOrderTriggerHandler{

    Set<id> SetSAIds = new set<id>(); 
    List<WorkOrderLineItem> listWOLI=new list<WorkOrderLineItem>();
    Map<string,WorkOrder> MapOppWO=new  Map<string,WorkOrder>();
    Map<string,List<Asset>> MapListAsset=new Map<string,List<Asset>>();
    List<WorkOrder> listWO=new list<WorkOrder>();
    
    public void OnInsert(List<WorkOrder> newWO){
           
        for(WorkOrder ObjWO:newWO){
            if(ObjWO.Opportunity__c!=null ){
                SetSAIds.add(ObjWO.Opportunity__c);
                listWO.add(ObjWO);
                system.debug('***'+SetSAIds);
            }
        }
        SetSAIds.remove(null);
        CreateWorkOrderLineItem(listWO);
    } 
          
    public void OnUpdate(List<WorkOrder> newWO,Map<id,WorkOrder> OldMapWO){
        Set<Id> workOrderIds = new Set<Id>();   
        for(WorkOrder ObjWO:newWO){
            if(ObjWO.Opportunity__c!=null && ObjWO.Opportunity__c!=OldMapWO.get(ObjWO.id).Opportunity__c){
                SetSAIds.add(ObjWO.Opportunity__c);
                listWO.add(ObjWO);
                system.debug('***'+SetSAIds);
            }
            System.debug('ObjWO @>'+ObjWO);
            integer oldNoOfTechCount = (OldMapWO.get(ObjWO.id).Number_of_Techs__c!=null ? Integer.valueOf(OldMapWO.get(ObjWO.id).Number_of_Techs__c) : 0);
            if(ObjWO.Number_of_Techs__c > oldNoOfTechCount && ObjWO.WorkTypeId!=null){
                workOrderIds.add(ObjWO.Id);
            }
        }
        SetSAIds.remove(null);
        CreateWorkOrderLineItem(listWO);
        if(!workOrderIds.isEmpty())
            cloneServiceAppointments(workOrderIds);
    } 
           
    public void CreateWorkOrderLineItem(List<WorkOrder> newWO){
        for(Opportunity ObjOpp:[select id,(select id,name,Room_Code__c,Description from Assets__r) from Opportunity where id In:SetSAIds]){         
            MapListAsset.put(ObjOpp.id,ObjOpp.Assets__r);
        }
      
        for(WorkOrder ObjWO:newWO){
            if(ObjWO.Opportunity__c!=null){         
                for(Asset ObjAsset:MapListAsset.get(ObjWO.Opportunity__c)){              
                    System.debug('***1'+ ObjAsset);                       
                    WorkOrderLineItem ObjWOLI=new WorkOrderLineItem();
                    ObjWOLI.Assetid=ObjAsset.id;
                    ObjWOLI.Subject=ObjWO.Subject;
                    ObjWOLI.Status='New';
                    ObjWOLI.WorkOrderid=ObjWO.id;
                    ObjWOLI.Room_Code__c=ObjAsset.Room_Code__c;
                    ObjWOLI.Description=ObjAsset.Description;
                    ObjWOLI.StartDate=ObjWO.StartDate;  
                    listWOLI.add(ObjWOLI);
                }
            }
        }     
        
        if(!listWOLI.isEmpty()){
            System.debug('****123'+listWOLI.size());
            insert listWOLI;
        } 
    }
    
    /**
        MethodName  : cloneServiceAppointments
        input workOrderIds: set of WO ids in which old SA needs to be created
        Description : method to create SA records 
    **/ 
    public void cloneServiceAppointments(Set<Id> workOrderIds){
        Map<Id,ServiceAppointment> woRelatedServiceAppMap = new Map<Id,ServiceAppointment>();
        Map<Id,Integer> woSaCount = new Map<Id,Integer>();
        List<ServiceAppointment> saList = new List<ServiceAppointment>();        
        System.debug('workOrderIds @>'+workOrderIds);
        //check count of SA in WO and fetch latest SA record
        for(ServiceAppointment sa: [Select Id, 
                                    ParentRecordId,
                                    ParentRecordType,
                                    Subject,
                                    Status,
                                    FSL__InternalSLRGeolocation__Latitude__s,
                                    FSL__InternalSLRGeolocation__Longitude__s,
                                    Street,
                                    City,
                                    State,
                                    Country,
                                    EarliestStartTime,
                                    DueDate,
                                    Duration,
                                    DurationType,
                                    Description,
                                    Work_Order__c,
                                    Work_Order__r.Number_of_Techs__c,
                                    Account_Name__c,
                                    AccountId
                                    FROM ServiceAppointment 
                                    WHERE Work_Order__c IN:workOrderIds 
                                    Order By CreatedDate DESC]){
            System.debug('sa @>'+sa);
            if(!woRelatedServiceAppMap.containsKey(sa.Work_Order__c)){
                woRelatedServiceAppMap.put(sa.Work_Order__c,sa);
                woSaCount.put(sa.Work_Order__c,1);
            } else {
                woSaCount.put(sa.Work_Order__c,(woSaCount.get(sa.Work_Order__c))+1);                                
            }           
        }
        
        //close latest SA record (new Number_of_Techs - old Number_of_Techs) times
        ServiceAppointment newSa;
        for(ServiceAppointment sa: woRelatedServiceAppMap.values()){
            for(Integer i=1;i <= (sa.Work_Order__r.Number_of_Techs__c - woSaCount.get(sa.Work_Order__c));i++){
                newSa = sa.clone(false, false, false, false);
                newSa.Status = 'None';
                saList.add(newSa);
            }
        }
        
        //insert SA records
        if(!saList.isEmpty()){
            checkRecursive.isUpdate=false;
            insert saList;              
        }
        System.debug('saList @>'+saList);
    }   
}